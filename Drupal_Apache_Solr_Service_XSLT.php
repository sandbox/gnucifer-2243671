<?php
class DrupalApacheSolrServiceXSLT extends DrupalApacheSolrService {


  /**
   * Simple Search interface
   *
   * @param string $query The raw query string
   * @param array $params key / value pairs for other query parameters (see Solr documentation), use arrays for parameter keys used more than once (e.g. facet.field)
   *
   * @return response object
   *
   * @throws Exception If an error occurs during the service call
   */
  public function search($query = '', $params = array(), $method = 'GET') {
    if (!is_array($params)) {
      $params = array();
    }
    $params['wt'] = 'xslt';
    $apachesolr_xslt_conf = apachesolr_environment_variable_get($this->env_id, 'apachesolr_xslt', array());
    $params += array(
      'tr' => $apachesolr_xslt_conf['xslt_template'],
    );
    if ($query) {
      $params['q'] = $query;
    }
    $response = $this->solrRequest($params);
    return $this->parseSolrResponse($response);
  }

  protected function parseSolrResponse($response) {
    if(!empty($response->data)) {
      //libxml_use_internal_errors(true);
      //$time = microtime(true);
      $xml = new SimpleXMLIterator($response->data);
      //dsm((microtime(true) - $time) * 1000);
      $errors = libxml_get_errors();
      libxml_use_internal_errors(false);
      if(empty($errors)) {
        //@todo: catch parse errors etc
        $response->response = new stdClass();
        foreach($xml->result->attributes() as $key => $value) {
          $response->response->{$key} = (string) $value;
        }
        $response->response->docs = (string) $xml->result;
        unset($xml->result);
        $parsed_result = $this->_parseSolrResponse($xml);
        foreach(get_object_vars($parsed_result) as $key => $value) {
          $response->{$key} = $value;
        }
      }
      else {
        $error = current($errors);
        //@todo: inform of use of correct format?
        $message = "Error parsing Solr response xml: " . $error->message;
        drupal_set_message($message, 'error');
        throw new Exception($message);
      }
    }
    return $response;
  }

  private function _parseSolrResponse($xml_iterator) {
    if($xml_iterator->getName() == 'arr') {
      $result = array();
      for($xml_iterator->rewind(); $xml_iterator->valid(); $xml_iterator->next()) {
        $current = $xml_iterator->current();
        if($xml_iterator->hasChildren()) {
          $result[] = $this->_parseSolrResponse($current);
        }
        else {
          $result[] = strval($current);
        }
      }
    }
    else {
      $result = new StdClass();
      for($xml_iterator->rewind(); $xml_iterator->valid(); $xml_iterator->next()) {
        $current = $xml_iterator->current();
        $name = (string) $current['name'];
        if(!empty($name)) {
          if($xml_iterator->hasChildren()) {
            $result->{$name} = $this->_parseSolrResponse($current);
          }
          else {
            $result->{$name} = $current->getName() !== 'null' ? strval($current): NULL;
          }
        }
        else {
          //simplexml sucks, @attributes wft!!?
        }
      }
    }
    return $result;
  }

}
